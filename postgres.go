package psql

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/jackc/pgx/v5"
	_ "github.com/jackc/pgx/v5/stdlib" // pgx driver
	"github.com/pkg/errors"
	"github.com/xegcrbq/logger"
)

var sqlFC = 3

type DB interface {
	Query(ctx context.Context, sql string, args ...any) (pgx.Rows, error)
	QueryRow(ctx context.Context, sql string, args ...any) pgx.Row
}

func SelectStruct[T any](ctx logger.ICLg, pool DB, sql string, args ...any) (t []T, err error) {
	var rows pgx.Rows
	{
		ctx := logger.Ctx(ctx, dbOperation, "Postgres")
		defer ctx.End()
		ctx.SpanSetKV("REQ", sqlStr(sql, args...))
		ctx.Skip(sqlFC).Tracef("SQL " + sqlStr(sql, args...))
		if rows, err = pool.Query(ctx, sql, args...); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			rows.Close()
			return
		}
		if t, err = pgx.CollectRows(rows, pgx.RowToStructByName[T]); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			rows.Close()
			return
		}
		rows.Close()
		d, _ := json.Marshal(t)
		ctx.SpanSetKV("RESP", t)
		ctx.Skip(sqlFC).Tracef("SQL " + string(d))
	}
	return
}

func SelectSimple[T any](ctx logger.ICLg, pool DB, sql string, args ...any) (t []T, err error) {
	var rows pgx.Rows
	{
		ctx := logger.Ctx(ctx, dbOperation, "Postgres")
		defer ctx.End()
		ctx.SpanSetKV("REQ", sqlStr(sql, args...))
		ctx.Skip(sqlFC).Tracef("SQL " + sqlStr(sql, args...))
		if rows, err = pool.Query(ctx, sql, args...); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			rows.Close()
			return
		}
		if t, err = pgx.CollectRows(rows, pgx.RowTo[T]); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			rows.Close()
			return
		}
		rows.Close()
		d, _ := json.Marshal(t)
		ctx.SpanSetKV("RESP", t)
		ctx.Skip(sqlFC).Tracef("SQL " + string(d))
	}
	return
}
func GetSimple[T any](ctx logger.ICLg, pool DB, sql string, args ...any) (t T, err error) {
	{
		ctx := logger.Ctx(ctx, dbOperation, "Postgres")
		defer ctx.End()
		ctx.SpanSetKV("REQ", sqlStr(sql, args...))
		ctx.Skip(sqlFC).Tracef("SQL " + sqlStr(sql, args...))
		if err = pool.QueryRow(ctx, sql, args...).Scan(&t); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			return
		}
		d, _ := json.Marshal(t)
		ctx.SpanSetKV("RESP", t)
		ctx.Skip(sqlFC).Tracef("SQL " + string(d))
	}
	return
}

func GetStruct[T any](ctx logger.ICLg, pool DB, sql string, args ...any) (t T, err error) {
	var (
		rows pgx.Rows
	)
	{
		ctx := logger.Ctx(ctx, dbOperation, "Postgres")
		defer ctx.End()
		ctx.SpanSetKV("REQ", sqlStr(sql, args...))
		ctx.Skip(sqlFC).Tracef("SQL " + sqlStr(sql, args...))
		if rows, err = pool.Query(ctx, sql, args...); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			rows.Close()
			return
		}
		if t, err = pgx.CollectOneRow(rows, pgx.RowToStructByName[T]); err != nil {
			err = errors.WithStack(err)
			ctx.SpanLog("RESP", fmt.Sprintf("%+v", err))
			return
		}
		rows.Close()
		d, _ := json.Marshal(t)
		ctx.SpanSetKV("RESP", t)
		ctx.Skip(sqlFC).Tracef("SQL " + string(d))
	}
	return
}

const dbOperation = "DB"
