package psql

import (
	"fmt"
	"regexp"
	"strings"
)

func sqlStr(sql string, args ...any) string {
	for i, arg := range args {
		argStr := fmt.Sprintf("%v", arg)
		sql = strings.Replace(sql, fmt.Sprint("$", i+1), argStr, 1)
	}
	space := regexp.MustCompile(`\s+`)
	s := space.ReplaceAllString(sql, " ")
	sql = fmt.Sprintf("%s", s)
	return sql
}
